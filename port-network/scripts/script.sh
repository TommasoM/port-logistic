#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your first network (BYFN) end-to-end test"
echo
CHANNEL_NAME="$1"
DELAY="$2"
LANGUAGE="$3"
TIMEOUT="$4"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="15"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

#CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
CC_SRC_PATH="github.com/chaincode/logisticaPortuale/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/logisticaPortuale/node/"
fi

echo "Channel name : "$CHANNEL_NAME

# import utils
. scripts/utils.sh

createChannel() {
	setGlobals 0 hub

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx >&log.txt
		echo "creato il canale $CHANNEL_NAME"
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel \"$CHANNEL_NAME\" is created successfully ===================== "
	echo
}

joinChannel () {
	joinChannelWithRetry 0 hub
	echo "===================== peer0.hub joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 1 hub
	echo "===================== peer1.hub joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 2 hub
	echo "===================== peer2.hub joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 0 maersk
	echo "===================== peer0.maersk joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 1 maersk
	echo "===================== peer1.maersk joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 0 spinelli
	echo "===================== peer0.spinelli joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
	joinChannelWithRetry 1 spinelli
	echo "===================== peer1.spinelli joined on the channel \"$CHANNEL_NAME\" ===================== "
	sleep $DELAY
	echo
}

## Create channel
echo "Creating channel..."
createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for hub..."
updateAnchorPeers 0 hub
echo "Updating anchor peers for maersk..."
updateAnchorPeers 0 maersk
echo "Updating anchor peers for spinelli..."
updateAnchorPeers 0 spinelli

## Install chaincode on peer0.org1 and peer0.org2
echo "Installing chaincode on peer0.hub..."
installChaincode 0 hub
echo "Install chaincode on peer0.maersk..."
installChaincode 0 maersk
echo "Install chaincode on peer0.spinelli..."
installChaincode 0 spinelli

## Install chaincode on peer1.hub
echo "Installing chaincode on peer1.hub..."
installChaincode 1 hub

## Install chaincode on peer2.hub
echo "Installing chaincode on peer2.hub..."
installChaincode 2 hub

## Install chaincode on peer1.maersk
echo "Installing chaincode on peer1.maersk..."
installChaincode 1 maersk

## Install chaincode on peer1.spinelli
echo "Installing chaincode on peer1.spinelli..."
installChaincode 1 spinelli

# Instantiate chaincode on peer0.org2
# In questa operazione viene anche fatta l'inizializzazione del ledger con i dati di prova
echo "Instantiating chaincode on peer0.hub..."
instantiateChaincode 0 hub

sleep 10

echo "Querying all vermas chaincode on peer0.hub..."
chaincodeQueryAllVermas 0 hub

echo "Sending invoke transaction on peer0.org1..."
chaincodeChangeVermasOwner 0 hub Ver1 OP1 OP5

sleep 5

echo "Querying all vermas chaincode on peer0.hub..."
chaincodeQueryAllVermas 0 hub

echo "Querying all bol chaincode on peer0.hub..."
chaincodeQueryAllBoL 0 hub

echo "Sending invoke transaction on peer0.org1..."
chaincodeChangeBoLOwner 0 hub BoL1 OP1 OP5

sleep 5

echo "Querying all vermas chaincode on peer0.hub..."
chaincodeQueryAllBoL 0 hub

echo "Querying all vermas owned chaincode on peer0.hub..."
chaincodeQueryVermasOwned 0 hub OP1

echo "Querying all bol owned chaincode on peer0.hub..."
chaincodeQueryBoLOwned 0 hub OP1

echo "Querying all vermas owned chaincode on peer0.hub..."
chaincodeQueryVermasOwned 0 hub OP2

echo "Querying all bol owned chaincode on peer0.hub..."
chaincodeQueryBoLOwned 0 hub OP2

echo
echo "========= All GOOD, BYFN execution completed =========== "
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

exit 0
