package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	//"bytes"
	"encoding/json"
	"fmt"
	//"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

type Operator struct {
	OperatorId 	string `json:"operatorId"`
	Email   	string `json:"email"`
	Name  		string `json:"name"`
}

type Vermas struct {
	VermasId	string `json:"vermasId"`
	Date		string `json:"date"`
	Text		string `json:"text"`
	Owner		string `json:"owner"`
}

/*
 * The Init method is called when the Smart Contract is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "queryOperator" {
		return s.queryOperator(APIstub, args)
	} else if function == "queryVermas" {
		return s.queryVermas(APIstub, args)
	} else if function == "createOperator" {
		return s.createOperator(APIstub, args)
	} else if function == "createVermas" {
		return s.createVermas(APIstub, args)
	}


	/////////////////////////////////////////////////////////////////////
	else if function == "invoke" {
		// Make payment of X units from A to B
		return t.invoke(stub, args)
	} else if function == "delete" {
		// Deletes an entity from its state
		return t.delete(stub, args)
	} else if function == "query" {
		// the old "Query" is now implemtned in invoke
		return t.query(stub, args)
	}
	/////////////////////////////////////////////////////////////////////
	return shim.Error("Invalid Smart Contract function name.")
}

///////////////////////////////////////////// INIZIO AGGIUNTA ////////////////////////////////////////////////

func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("ex02 Init")
	_, args := stub.GetFunctionAndParameters()
	var A, B string    // Entities
	var Aval, Bval int // Asset holdings
	var err error

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	// Initialize the chaincode
	A = args[0]
	Aval, err = strconv.Atoi(args[1])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}
	B = args[2]
	Bval, err = strconv.Atoi(args[3])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)

	// Write the state to the ledger
	err = stub.PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

// query callback representing the query of a chaincode
func (t *SimpleChaincode) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var A string // Entities
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}

	A = args[0]

	// Get the state from the ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	if Avalbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + A + "\",\"Amount\":\"" + string(Avalbytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return shim.Success(Avalbytes)
}

func (t *SimpleChaincode) invoke(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var A, B string    // Entities
	var Aval, Bval int // Asset holdings
	var X int          // Transaction value
	var err error

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	A = args[0]
	B = args[1]

	// Get the state from the ledger
	// TODO: will be nice to have a GetAllState call to ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Avalbytes == nil {
		return shim.Error("Entity not found")
	}
	Aval, _ = strconv.Atoi(string(Avalbytes))

	Bvalbytes, err := stub.GetState(B)
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Bvalbytes == nil {
		return shim.Error("Entity not found")
	}
	Bval, _ = strconv.Atoi(string(Bvalbytes))

	// Perform the execution
	X, err = strconv.Atoi(args[2])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}
	Aval = Aval - X
	Bval = Bval + X
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)

	// Write the state back to the ledger
	err = stub.PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

// Deletes an entity from state
func (t *SimpleChaincode) delete(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	A := args[0]

	// Delete the key from the state in ledger
	err := stub.DelState(A)
	if err != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

/////////////////////////////////////////// FINE AGGIUNTA //////////////////////////////////////////////////

/*
----------------------------QUERIES----------------------------
*/


func (s *SmartContract) queryOperator(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting OperatorId")
	}

	operatorAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(operatorAsBytes)
}

func (s *SmartContract) queryVermas(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting VermasId")
	}

	vermasAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(vermasAsBytes)
}

/*
----------------------------CREATES----------------------------
*/

func (s *SmartContract) createOperator (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting OperatorId, Email, Name")
	}

	var operator = Operator{OperatorId: args[0], Email: args[1], Name: args[2]}

	operatorAsBytes, _ := json.Marshal(operator)
	//Il primo parametro è la chiave, dunque l'indirizzo email
	APIstub.PutState(args[0], operatorAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) createVermas (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting VermasId, Date, Text, Owner")
	}

	var vermas = Vermas{VermasId: args[0], Date: args[1], Text: args[2], Owner: args[3]}

	vermasAsBytes, _ := json.Marshal(vermas)
	//Il primo parametro è la chiave, dunque il PlaneId
	APIstub.PutState(args[0], vermasAsBytes)

	return shim.Success(nil)
}



/*
----------------------------INIT LEDGER----------------------------
*/

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	operators := []Operator{
		Operator{OperatorId: "OP0",	Email: "op0@email",	Name: "Alex"},
		Operator{OperatorId: "OP1",	Email: "op1@email",	Name: "Ben"},
		Operator{OperatorId: "OP2",	Email: "op2@email",	Name: "Carol"},
		Operator{OperatorId: "OP3",	Email: "op3@email",	Name: "Denise"},
		Operator{OperatorId: "OP4",	Email: "op4@email",	Name: "Erik"},
		Operator{OperatorId: "OP5",	Email: "op5@email",	Name: "Frank"},
		Operator{OperatorId: "OP6",	Email: "op6@email",	Name: "Giulia"},
		Operator{OperatorId: "OP7",	Email: "op7@email",	Name: "Hodor"},
		Operator{OperatorId: "OP8",	Email: "op8@email",	Name: "Irene"},
		Operator{OperatorId: "OP9",	Email: "op9@email",	Name: "Luke"},
	}

	vermas := []Vermas{
		Vermas{VermasId: "Ver0",	Date: "12/06/2018",	Text: "testo vermas Ver0",	Owner: "OP0"},
		Vermas{VermasId: "Ver1",	Date: "12/06/2018",	Text: "testo vermas Ver1",	Owner: "OP1"},
		Vermas{VermasId: "Ver2",	Date: "12/06/2018",	Text: "testo vermas Ver2",	Owner: "OP2"},
		Vermas{VermasId: "Ver3",	Date: "12/06/2018",	Text: "testo vermas Ver3",	Owner: "OP3"},
		Vermas{VermasId: "Ver4",	Date: "11/06/2018",	Text: "testo vermas Ver4",	Owner: "OP4"},
		Vermas{VermasId: "Ver5",	Date: "11/06/2018",	Text: "testo vermas Ver5",	Owner: "OP5"},
		Vermas{VermasId: "Ver6",	Date: "10/06/2018",	Text: "testo vermas Ver6",	Owner: "OP6"},
		Vermas{VermasId: "Ver7",	Date: "10/06/2018",	Text: "testo vermas Ver7",	Owner: "OP7"},
		Vermas{VermasId: "Ver8",	Date: "8/06/2018",	Text: "testo vermas Ver8",	Owner: "OP8"},
		Vermas{VermasId: "Ver9",	Date: "8/06/2018",	Text: "testo vermas Ver9",	Owner: "OP9"},
		Vermas{VermasId: "Ver10",	Date: "8/06/2018",	Text: "testo vermas Ver10",	Owner: "OP0"},
		Vermas{VermasId: "Ver11",	Date: "7/06/2018",	Text: "testo vermas Ver11",	Owner: "OP1"},
		Vermas{VermasId: "Ver12",	Date: "6/06/2018",	Text: "testo vermas Ver12",	Owner: "OP2"},
		Vermas{VermasId: "Ver13",	Date: "6/06/2018",	Text: "testo vermas Ver13",	Owner: "OP3"},
		Vermas{VermasId: "Ver14",	Date: "4/06/2018",	Text: "testo vermas Ver14",	Owner: "OP4"},
		Vermas{VermasId: "Ver15",	Date: "4/06/2018",	Text: "testo vermas Ver15",	Owner: "OP5"},
		Vermas{VermasId: "Ver16",	Date: "4/06/2018",	Text: "testo vermas Ver16",	Owner: "OP6"},
		Vermas{VermasId: "Ver17",	Date: "2/06/2018",	Text: "testo vermas Ver17",	Owner: "OP7"},
		Vermas{VermasId: "Ver18",	Date: "1/06/2018",	Text: "testo vermas Ver18",	Owner: "OP8"},
		Vermas{VermasId: "Ver19",	Date: "1/06/2018",	Text: "testo vermas Ver19",	Owner: "OP9"},
		Vermas{VermasId: "Ver20",	Date: "31/05/2018",	Text: "testo vermas Ver20",	Owner: "OP0"},
		Vermas{VermasId: "Ver21",	Date: "28/05/2018",	Text: "testo vermas Ver21",	Owner: "OP1"},
		Vermas{VermasId: "Ver22",	Date: "28/05/2018",	Text: "testo vermas Ver22",	Owner: "OP2"},
		Vermas{VermasId: "Ver23",	Date: "27/05/2018",	Text: "testo vermas Ver23",	Owner: "OP3"},
	}
	
	i := 0
	for i < len(operators) {
		fmt.Println("i is ", i)
		operatorAsBytes, _ := json.Marshal(operators[i])
		APIstub.PutState(operators[i].OperatorId, operatorAsBytes)
		fmt.Println("Added", operators[i])
		i = i + 1
	}

	i = 0
	for i < len(vermas) {
		fmt.Println("i is ", i)
		vermasAsBytes, _ := json.Marshal(vermas[i])
		APIstub.PutState(vermas[i].VermasId, vermasAsBytes)
		fmt.Println("Added", vermas[i])
		i = i + 1
	}


	return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
