package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

type Operator struct {
	Email   	string `json:"email"`
	Name  		string `json:"name"`
	Surname		string `json:"surname"`
	Location 	string `json:"location"`
	Role  		string `json:"role"`
}

type Task struct {
	TaskId		string	`json:"taskId"`
	Name		string	`json:"name"`
	Description	string	`json:"description"`
	LoEToTutor	string	`json:"LoEToTutor"`
}

type Expertise struct {
	ExpertiseId	string		`json:"expertiseId"`
	Operator	Operator	`json:"operator"`
	Task		string		`json:"task"`
	LoE			string		`json:"LoE"`
}

type Plane struct {
	PlaneId		string `json:"planeId"`
	Model		string `json:"molde"`
}

//MRT = Meintenance Request Table
type MRT struct {
	MRTId			string		`json:"taskId"`
	Maintainer		string		`json:"maintainer"`
	Manager			string		`json:"manager"`
	FieldOperator	string		`json:"fieldOperator"`
	Tutor			string		`json:"tutor"`
	Plane			string 		`json:"plane"`
	Task			string 		`json:"stringaDiTask"`
	Location		string		`json:"location"`
	Report			string 		`json:"report"`
}

/*
 * The Init method is called when the Smart Contract is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "queryOperator" {
		return s.queryOperator(APIstub, args)
	} else if function == "queryExpertise" {
		return s.queryExpertise(APIstub, args)
	} else if function == "queryTaskExpertises" {
		return s.queryTaskExpertises(APIstub, args)
	} else if function == "queryFieldOperatorExpertises" {
		return s.queryFieldOperatorExpertises(APIstub, args)
	} else if function == "queryTutorExpertises" {
		return s.queryTutorExpertises(APIstub, args)
	} else if function == "queryMRT" {
		return s.queryMRT(APIstub)
	} else if function == "queryAllOperators" {
		return s.queryAllOperators(APIstub)
	} else if function == "queryAllFieldOperators" {
		return s.queryAllFieldOperators(APIstub)
	} else if function == "queryAllTutors" {
		return s.queryAllTutors(APIstub)
	} else if function == "queryAllManagers" {
		return s.queryAllManagers(APIstub)
	} else if function == "queryAllMaintainers" {
		return s.queryAllMaintainers(APIstub)
	} else if function == "createOperator" {
		return s.createOperator(APIstub, args)
	} else if function == "createPlane" {
		return s.createPlane(APIstub, args)
	} else if function == "createTask" {
		return s.createTask(APIstub, args)
	} else if function == "createExpertise" {
		return s.createExpertise(APIstub, args)
	} else if function == "createMRT" {
		return s.createMRT(APIstub, args)
	}


	return shim.Error("Invalid Smart Contract function name.")
}

/*
----------------------------QUERIES----------------------------
*/


func (s *SmartContract) queryOperator(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	operatorAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(operatorAsBytes)
}

func (s *SmartContract) queryExpertise(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting an Email, TaskId")
	}

	queryString := "{\"selector\":{\"$and\":[{\"operator\":args[0]},{\"task\":args[1]}]}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryTaskExpertises(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting a TaskId")
	}

	queryString := "{\"selector\":{\"task\":args[0]}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryFieldOperatorExpertises(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting a TaskId")
	}

	queryString := "{\"selector\":{\"$and\":[{\"operator.role\":\"FieldOperator\"},{\"task\":args[0]}]}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryTutorExpertises(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting a TaskId")
	}

	queryString := "{\"selector\":{\"$and\":[{\"operator.role\":\"Tutor\"},{\"task\":args[1]}]}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryMRT(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting MRTId")
	}

	MRTAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(MRTAsBytes)
}

func (s *SmartContract) queryAllOperators(APIstub shim.ChaincodeStubInterface) sc.Response {
	queryString := "{\"selector\":{\"location\":\"Genova\"}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAllFieldOperators(APIstub shim.ChaincodeStubInterface) sc.Response {
	queryString := "{\"selector\":{\"role\":\"FieldOperator\"}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAllTutors(APIstub shim.ChaincodeStubInterface) sc.Response {
	queryString := "{\"selector\":{\"role\":\"Tutor\"}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAllManagers(APIstub shim.ChaincodeStubInterface) sc.Response {
	queryString := "{\"selector\":{\"role\":\"Manager\"}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("Query error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
            return shim.Error("Iterator instantiate error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAllMaintainers(APIstub shim.ChaincodeStubInterface) sc.Response {
	queryString := "{\"selector\":{\"role\":\"maintainer\"}}"
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
    resultsIterator, err := APIstub.GetQueryResult(queryString)
    defer resultsIterator.Close()
    if err != nil {
        return shim.Error("Query error")
    }
    // buffer is a JSON array containing QueryRecords
    var buffer bytes.Buffer
    buffer.WriteString("[")
    bArrayMemberAlreadyWritten := false
    for resultsIterator.HasNext() {
        queryResponse,
        err := resultsIterator.Next()
        if err != nil {
             return shim.Error("Iterator instantiate error")
        }
        // Add a comma before array members, suppress it for the first array member
        if bArrayMemberAlreadyWritten == true {
            buffer.WriteString(",")
        }
        buffer.WriteString("{\"Key\":")
        buffer.WriteString("\"")
        buffer.WriteString(queryResponse.Key)
        buffer.WriteString("\"")
        buffer.WriteString(", \"Record\":")
        // Record is a JSON object, so we write as-is
        buffer.WriteString(string(queryResponse.Value))
        buffer.WriteString("}")
        bArrayMemberAlreadyWritten = true
    }
    buffer.WriteString("]")
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
    return shim.Success(buffer.Bytes())
}

/*
----------------------------CREATES----------------------------
*/

func (s *SmartContract) createOperator (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting Email, Name, Surname, Location, Role")
	}

	var operator = Operator{Email: args[0], Name: args[1], Surname: args[2], Location: args[3], Role: args[4]}

	operatorAsBytes, _ := json.Marshal(operator)
	//Il primo parametro è la chiave, dunque l'indirizzo email
	APIstub.PutState(args[0], operatorAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) createPlane (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting PlaneId, Model")
	}

	var plane = Plane{PlaneId: args[0], Model: args[1]}

	planeAsBytes, _ := json.Marshal(plane)
	//Il primo parametro è la chiave, dunque il PlaneId
	APIstub.PutState(args[0], planeAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) createTask (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting TaskId, Name, Description, LoEToTutor")
	}

	var task = Task{TaskId: args[0], Name: args[1], Description: args[2], LoEToTutor: args[3]}

	taskAsBytes, _ := json.Marshal(task)
	//Il primo parametro è la chiave, dunque il TaskId
	APIstub.PutState(args[0], taskAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) createExpertise (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting ExpertiseId, Operator, Task, LoE")
	}

	var expertise = Expertise{ExpertiseId: args[0], Operator: args[1], Task: args[2], LoE: args[3]}

	expertiseAsBytes, _ := json.Marshal(expertise)
	//Il primo parametro è la chiave, dunque l'ExpertiseId
	APIstub.PutState(args[0], expertiseAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) createMRT (APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 9 {
		return shim.Error("Incorrect number of arguments. Expecting MRTId, Maintainer, Manager, FieldOperator, Tutor, Plane, Task, Location, Report")
	}

	var mrt = MRT{MRTId: args[0], Maintainer: args[1], Manager: args[2], FieldOperator: args[3], Tutor: args[4], Plane: args[5], Task: args[6], Location: args[7], Report: args[8]}

	mrtAsBytes, _ := json.Marshal(mrt)
	//Il primo parametro è la chiave, dunque l'MRTId
	APIstub.PutState(args[0], mrtAsBytes)

	return shim.Success(nil)
}

/*
----------------------------INIT LEDGER----------------------------
*/

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	operators := []Operator{
		Operator{Email: "tommaso.martin@outlook.it", Name: "Tommaso", Surname: "Martin", Location: "Genova", Role: "FieldOperator"},
		Operator{Email: "g.camera@m3s.it", Name: "Giancarlo", Surname: "Camera", Location: "Genova", Role: "FieldOperator"},
		Operator{Email: "gc_91@live.it", Name: "Carlo", Surname: "Andreotti", Location: "Genova", Role: "FieldOperator"},
		Operator{Email: "tommaso.martin555@gmail.com", Name: "Luca", Surname: "Gaggero", Location: "Genova", Role: "Maintainer"},
		Operator{Email: "giancarlo.camera@m3s.it", Name: "Matteo", Surname: "Serratore", Location: "Genova", Role: "Maintainer"},
		Operator{Email: "g.camera@cipi.unige.it", Name: "Pierpaolo", Surname: "Baglietto", Location: "Genova", Role: "Manager"},
		Operator{Email: "camera@m3s.it", Name: "Maurilio", Surname: "Cuneo", Location: "Genova", Role: "Tutor"},
		Operator{Email: "camera.giancarlo@gmail.com", Name: "Andrea", Surname: "Parodi", Location: "Genova", Role: "Tutor"},
	}

	planes := []Plane{
		Plane{PlaneId: "planeId1", Model: "Eurofighter EF-2000 Typhoon"},
		Plane{PlaneId: "planeId2", Model: "Leonardo Velivoli P-72 Surveyor"},
		Plane{PlaneId: "planeId3", Model: "Eurofighter EF-2000 Typhoon"},
		Plane{PlaneId: "planeId4", Model: "Eurofighter EF-2000 Typhoon"},
		Plane{PlaneId: "planeId5", Model: "Leonardo Velivoli P-72 Surveyor"},
	}

	tasks := []Task{
		Task{TaskId: "taskId1", Name: "Sostituzione Turboventola", Description: "Sostituzione di una turboventola danneggiata o usurata", LoEToTutor: "7"},
		Task{TaskId: "taskId2", Name: "Riparazione carrello", Description: "Riparazione del carrello dannaggiato, usurato o difettoso", LoEToTutor: "6"},
		Task{TaskId: "taskId3", Name: "Sostituzione motore", Description: "Sostituzione del motore", LoEToTutor: "9"},
	}

	expertises := []Expertise{
		Expertise{ExpertiseId: "expertiseId1", Operator: "tommaso.martin@outlook.it", Task: "taskId1", LoE: "4"},
		Expertise{ExpertiseId: "expertiseId2", Operator: "tommaso.martin@outlook.it", Task: "taskId2", LoE: "5"},
		Expertise{ExpertiseId: "expertiseId3", Operator: "tommaso.martin@outlook.it", Task: "taskId3", LoE: "4"},
		Expertise{ExpertiseId: "expertiseId4", Operator: "g.camera@m3s.it", Task: "taskId1", LoE: "7"},
		Expertise{ExpertiseId: "expertiseId5", Operator: "g.camera@m3s.it", Task: "taskId3", LoE: "8"},
		Expertise{ExpertiseId: "expertiseId6", Operator: "gc_91@live.it", Task: "taskId1", LoE: "1"},
		Expertise{ExpertiseId: "expertiseId7", Operator: "gc_91@live.it", Task: "taskId2", LoE: "6"},
		Expertise{ExpertiseId: "expertiseId8", Operator: "gc_91@live.it", Task: "taskId3", LoE: "4"},
		Expertise{ExpertiseId: "expertiseId9", Operator: "camera@m3s.it", Task: "taskId2", LoE: "7"},
		Expertise{ExpertiseId: "expertiseId10", Operator: "camera.giancarlo@gmail.com", Task: "taskId1", LoE: "8"},
		Expertise{ExpertiseId: "expertiseId11", Operator: "camera.giancarlo@gmail.com", Task: "taskId2", LoE: "5"},
		Expertise{ExpertiseId: "expertiseId12", Operator: "camera.giancarlo@gmail.com", Task: "taskId3", LoE: "10"},
	}
	
	i := 0
	for i < len(operators) {
		fmt.Println("i is ", i)
		operatorAsBytes, _ := json.Marshal(operators[i])
		APIstub.PutState(operators[i].Name, operatorAsBytes)
		fmt.Println("Added", operators[i])
		i = i + 1
	}

	i = 0
	for i < len(planes) {
		fmt.Println("i is ", i)
		planeAsBytes, _ := json.Marshal(planes[i])
		APIstub.PutState(planes[i].PlaneId, planeAsBytes)
		fmt.Println("Added", planes[i])
		i = i + 1
	}

	i = 0
	for i < len(tasks) {
		fmt.Println("i is ", i)
		taskAsBytes, _ := json.Marshal(tasks[i])
		APIstub.PutState(tasks[i].TaskId, taskAsBytes)
		fmt.Println("Added", tasks[i])
		i = i + 1
	}

	i = 0
	for i < len(expertises) {
		fmt.Println("i is ", i)
		expertiseAsBytes, _ := json.Marshal(expertises[i])
		APIstub.PutState(expertises[i].ExpertiseId, expertiseAsBytes)
		fmt.Println("Added", expertises[i])
		i = i + 1
	}

	return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
