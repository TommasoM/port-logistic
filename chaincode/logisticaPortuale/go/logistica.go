/*
Copyright IBM Corp. 2016 All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

//WARNING - this chaincode's ID is hard-coded in chaincode_example04 to illustrate one way of
//calling chaincode from a chaincode. If this example is modified, chaincode_example04.go has
//to be modified as well with the new ID of chaincode_example02.
//chaincode_example05 show's how chaincode ID can be passed in as a parameter instead of
//hard-coding.

import (
	"fmt"
	"bytes"
	"strconv"
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type Operator struct {
	OperatorId 	string `json:"operatorId"`
	Email   	string `json:"email"`
	Name  		string `json:"name"`
}

type Vermas struct {
	VermasId	string `json:"vermasId"`
	Date		string `json:"date"`
	Text		string `json:"text"`
	Owner		string `json:"owner"`
}

type BoL struct {
	BoLId		string `json:"bolId"`
	Date		string `json:"date"`
	Text		string `json:"text"`
	Owner		string `json:"owner"`
}

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	operators := []Operator{
		Operator{OperatorId: "OP0",	Email: "op0@email",	Name: "Alex"},
		Operator{OperatorId: "OP1",	Email: "op1@email",	Name: "Ben"},
		Operator{OperatorId: "OP2",	Email: "op2@email",	Name: "Carol"},
		Operator{OperatorId: "OP3",	Email: "op3@email",	Name: "Denise"},
		Operator{OperatorId: "OP4",	Email: "op4@email",	Name: "Erik"},
		Operator{OperatorId: "OP5",	Email: "op5@email",	Name: "Frank"},
		Operator{OperatorId: "OP6",	Email: "op6@email",	Name: "Giulia"},
		Operator{OperatorId: "OP7",	Email: "op7@email",	Name: "Hodor"},
		Operator{OperatorId: "OP8",	Email: "op8@email",	Name: "Irene"},
		Operator{OperatorId: "OP9",	Email: "op9@email",	Name: "Luke"},
	}

	vermas := []Vermas{
		Vermas{VermasId: "Ver0",	Date: "12/06/2018",	Text: "testo vermas Ver0",	Owner: "OP0"},
		Vermas{VermasId: "Ver1",	Date: "12/06/2018",	Text: "testo vermas Ver1",	Owner: "OP1"},
		Vermas{VermasId: "Ver2",	Date: "12/06/2018",	Text: "testo vermas Ver2",	Owner: "OP2"},
		Vermas{VermasId: "Ver3",	Date: "12/06/2018",	Text: "testo vermas Ver3",	Owner: "OP3"},
		Vermas{VermasId: "Ver4",	Date: "11/06/2018",	Text: "testo vermas Ver4",	Owner: "OP4"},
		Vermas{VermasId: "Ver5",	Date: "11/06/2018",	Text: "testo vermas Ver5",	Owner: "OP5"},
		Vermas{VermasId: "Ver6",	Date: "10/06/2018",	Text: "testo vermas Ver6",	Owner: "OP6"},
		Vermas{VermasId: "Ver7",	Date: "10/06/2018",	Text: "testo vermas Ver7",	Owner: "OP7"},
		Vermas{VermasId: "Ver8",	Date: "8/06/2018",	Text: "testo vermas Ver8",	Owner: "OP8"},
		Vermas{VermasId: "Ver9",	Date: "8/06/2018",	Text: "testo vermas Ver9",	Owner: "OP9"},
		Vermas{VermasId: "Ver10",	Date: "8/06/2018",	Text: "testo vermas Ver10",	Owner: "OP0"},
		Vermas{VermasId: "Ver11",	Date: "7/06/2018",	Text: "testo vermas Ver11",	Owner: "OP1"},
		Vermas{VermasId: "Ver12",	Date: "6/06/2018",	Text: "testo vermas Ver12",	Owner: "OP2"},
		Vermas{VermasId: "Ver13",	Date: "6/06/2018",	Text: "testo vermas Ver13",	Owner: "OP3"},
		Vermas{VermasId: "Ver14",	Date: "4/06/2018",	Text: "testo vermas Ver14",	Owner: "OP4"},
		Vermas{VermasId: "Ver15",	Date: "4/06/2018",	Text: "testo vermas Ver15",	Owner: "OP5"},
		Vermas{VermasId: "Ver16",	Date: "4/06/2018",	Text: "testo vermas Ver16",	Owner: "OP6"},
		Vermas{VermasId: "Ver17",	Date: "2/06/2018",	Text: "testo vermas Ver17",	Owner: "OP7"},
		Vermas{VermasId: "Ver18",	Date: "1/06/2018",	Text: "testo vermas Ver18",	Owner: "OP8"},
		Vermas{VermasId: "Ver19",	Date: "1/06/2018",	Text: "testo vermas Ver19",	Owner: "OP9"},
		Vermas{VermasId: "Ver20",	Date: "31/05/2018",	Text: "testo vermas Ver20",	Owner: "OP0"},
		Vermas{VermasId: "Ver21",	Date: "28/05/2018",	Text: "testo vermas Ver21",	Owner: "OP1"},
		Vermas{VermasId: "Ver22",	Date: "28/05/2018",	Text: "testo vermas Ver22",	Owner: "OP2"},
		Vermas{VermasId: "Ver23",	Date: "27/05/2018",	Text: "testo vermas Ver23",	Owner: "OP3"},
	}

	bols := []BoL{
		BoL{BoLId: "BoL0",	Date: "12/06/2018",	Text: "testo BoL BoL0",		Owner: "OP0"},
		BoL{BoLId: "BoL1",	Date: "12/06/2018",	Text: "testo BoL BoL1",		Owner: "OP1"},
		BoL{BoLId: "BoL2",	Date: "12/06/2018",	Text: "testo BoL BoL2",		Owner: "OP2"},
		BoL{BoLId: "BoL3",	Date: "12/06/2018",	Text: "testo BoL BoL3",		Owner: "OP3"},
		BoL{BoLId: "BoL4",	Date: "11/06/2018",	Text: "testo BoL BoL4",		Owner: "OP4"},
		BoL{BoLId: "BoL5",	Date: "11/06/2018",	Text: "testo BoL BoL5",		Owner: "OP5"},
		BoL{BoLId: "BoL6",	Date: "10/06/2018",	Text: "testo BoL BoL6",		Owner: "OP6"},
		BoL{BoLId: "BoL7",	Date: "10/06/2018",	Text: "testo BoL BoL7",		Owner: "OP7"},
		BoL{BoLId: "BoL8",	Date: "8/06/2018",	Text: "testo BoL BoL8",		Owner: "OP8"},
		BoL{BoLId: "BoL9",	Date: "8/06/2018",	Text: "testo BoL BoL9",		Owner: "OP9"},
		BoL{BoLId: "BoL10",	Date: "8/06/2018",	Text: "testo BoL BoL10",	Owner: "OP0"},
		BoL{BoLId: "BoL11",	Date: "7/06/2018",	Text: "testo BoL BoL11",	Owner: "OP1"},
		BoL{BoLId: "BoL12",	Date: "6/06/2018",	Text: "testo BoL BoL12",	Owner: "OP2"},
		BoL{BoLId: "BoL13",	Date: "6/06/2018",	Text: "testo BoL BoL13",	Owner: "OP3"},
		BoL{BoLId: "BoL14",	Date: "4/06/2018",	Text: "testo BoL BoL14",	Owner: "OP4"},
		BoL{BoLId: "BoL15",	Date: "4/06/2018",	Text: "testo BoL BoL15",	Owner: "OP5"},
		BoL{BoLId: "BoL16",	Date: "4/06/2018",	Text: "testo BoL BoL16",	Owner: "OP6"},
		BoL{BoLId: "BoL17",	Date: "2/06/2018",	Text: "testo BoL BoL17",	Owner: "OP7"},
		BoL{BoLId: "BoL18",	Date: "1/06/2018",	Text: "testo BoL BoL18",	Owner: "OP8"},
		BoL{BoLId: "BoL19",	Date: "1/06/2018",	Text: "testo BoL BoL19",	Owner: "OP9"},
		BoL{BoLId: "BoL20",	Date: "31/05/2018",	Text: "testo BoL BoL20",	Owner: "OP0"},
		BoL{BoLId: "BoL21",	Date: "28/05/2018",	Text: "testo BoL BoL21",	Owner: "OP1"},
		BoL{BoLId: "BoL22",	Date: "28/05/2018",	Text: "testo BoL BoL22",	Owner: "OP2"},
		BoL{BoLId: "BoL23",	Date: "27/05/2018",	Text: "testo BoL BoL23",	Owner: "OP3"},
	}

	i := 0
	for i < len(operators) {
		fmt.Println("i is ", i)
		operatorAsBytes, _ := json.Marshal(operators[i])
		stub.PutState(operators[i].OperatorId, operatorAsBytes)
		fmt.Println("Added", operators[i])
		i = i + 1
	}

	i = 0
	for i < len(vermas) {
		fmt.Println("i is ", i)
		vermasAsBytes, _ := json.Marshal(vermas[i])
		stub.PutState(vermas[i].VermasId, vermasAsBytes)
		fmt.Println("Added", vermas[i])
		i = i + 1
	}

	i = 0
	for i < len(bols) {
		fmt.Println("i is ", i)
		bolAsBytes, _ := json.Marshal(bols[i])
		stub.PutState(bols[i].BoLId, bolAsBytes)
		fmt.Println("Added", bols[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Esecuzione Invoke")
	function, args := stub.GetFunctionAndParameters()
	if function == "changeVermasOwner" {
		return t.changeVermasOwner(stub, args)
	} else if function == "changeBoLOwner" {
		return t.changeBoLOwner(stub, args)
	} else if function == "delete" {
		return t.delete(stub, args)
	} else if function == "queryVermas" {
		return t.queryVermas(stub, args)
	} else if function == "queryAllVermas" {
		return t.queryAllVermas(stub)
	} else if function == "queryBoL" {
		return t.queryBoL(stub, args)
	} else if function == "queryAllBoL" {
		return t.queryAllBoL(stub)
	} else if function == "queryVermasOwned" {
		return t.queryVermasOwned(stub, args)
	} else if function == "queryBoLOwned" {
		return t.queryBoLOwned(stub, args)
	}

	return shim.Error("Invalid invoke function name. Expecting \"invoke\" \"delete\" \"query\"")
}

func (t *SimpleChaincode) changeVermasOwner(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var vermasId, oldOwner, newOwner string

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting vermasId, oldOwner, newOwner")
	}

	vermasId = args[0]
	oldOwner = args[1]
	newOwner = args[2]
	
	var buffer bytes.Buffer
	buffer.WriteString("Tentativo di change owner: \nVermasID: ")
	buffer.WriteString(vermasId)
	buffer.WriteString(" oldOwner: ")
	buffer.WriteString(oldOwner)
	buffer.WriteString(" newOwner: ")
	buffer.WriteString(newOwner)
	fmt.Printf("- changeVermasOwner:\n%s\n", buffer.String())

	vermasAsBytes, _ := stub.GetState(vermasId)
	ver := Vermas{}

	json.Unmarshal(vermasAsBytes, &ver)
	
	if ver.Owner == oldOwner {	
		ver.Owner = newOwner
	} else {
		return shim.Error("Per poter fare un cambio di proprietario la transazione deve essere eseguita dal proprietario corrente")
	}

	vermasAsBytes, _ = json.Marshal(ver)
	stub.PutState(vermasId, vermasAsBytes)

	return shim.Success(nil)
}

func (t *SimpleChaincode) changeBoLOwner(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var BoLId, oldOwner, newOwner string

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting BoLId, oldOwner, newOwner")
	}

	BoLId = args[0]
	oldOwner = args[1]
	newOwner = args[2]
	
	var buffer bytes.Buffer
	buffer.WriteString("Tentativo di change owner: \nBoLID: ")
	buffer.WriteString(BoLId)
	buffer.WriteString(" oldOwner: ")
	buffer.WriteString(oldOwner)
	buffer.WriteString(" newOwner: ")
	buffer.WriteString(newOwner)
	fmt.Printf("- changeBoLOwner:\n%s\n", buffer.String())

	BoLAsBytes, _ := stub.GetState(BoLId)
	bol := BoL{}

	json.Unmarshal(BoLAsBytes, &bol)
	
	if bol.Owner == oldOwner {	
		bol.Owner = newOwner
	} else {
		return shim.Error("Per poter fare un cambio di proprietario la transazione deve essere eseguita dal proprietario corrente")
	}

	BoLAsBytes, _ = json.Marshal(bol)
	stub.PutState(BoLId, BoLAsBytes)

	return shim.Success(nil)
}

func (t *SimpleChaincode) queryAllVermas(stub shim.ChaincodeStubInterface) pb.Response {

	startKey := "Ver0"
	endKey := "Ver99"

	resultsIterator, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("\n{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllVermas:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (t *SimpleChaincode) queryAllBoL(stub shim.ChaincodeStubInterface) pb.Response {

	startKey := "BoL0"
	endKey := "BoL99"

	resultsIterator, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("\n{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllBoL:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (t *SimpleChaincode) queryVermasOwned(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var owner, vermasIdSuffix, vermasId string
	fmt.Printf("\n\nqueryVermasOwned\n\n")
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}
	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false
	owner = args[0]
	vermasIdSuffix = "Ver"
	for i:=0; i<100; i++ {
		vermasId = vermasIdSuffix + strconv.Itoa(i)
		vermasAsBytes, err := stub.GetState(vermasId)
		if err == nil && vermasAsBytes != nil {
			ver := Vermas{}
			json.Unmarshal(vermasAsBytes, &ver)
			if ver.Owner == owner {
				if bArrayMemberAlreadyWritten == true {
					buffer.WriteString(",")
				}
				buffer.WriteString("\n{\"Key\":")
				buffer.WriteString("\"")
				buffer.WriteString(vermasId)
				buffer.WriteString("\"")
				buffer.WriteString(", \"Record\":")
				// Record is a JSON object, so we write as-is
				buffer.WriteString(string(vermasAsBytes))
				buffer.WriteString("}")
				bArrayMemberAlreadyWritten = true
			}
		}
	}
	return shim.Success(buffer.Bytes())
}

func (t *SimpleChaincode) queryBoLOwned(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var owner, BoLIdSuffix, BoLId string
	fmt.Printf("\n\nqueryBoLOwned\n\n")
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}
	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false
	owner = args[0]
	BoLIdSuffix = "BoL"
	for i:=0; i<100; i++ {
		BoLId = BoLIdSuffix + strconv.Itoa(i)
		BoLAsBytes, err := stub.GetState(BoLId)
		if err == nil && BoLAsBytes != nil {
			bol := BoL{}
			json.Unmarshal(BoLAsBytes, &bol)
			if bol.Owner == owner {
				if bArrayMemberAlreadyWritten == true {
					buffer.WriteString(",")
				}
				buffer.WriteString("\n{\"Key\":")
				buffer.WriteString("\"")
				buffer.WriteString(BoLId)
				buffer.WriteString("\"")
				buffer.WriteString(", \"Record\":")
				// Record is a JSON object, so we write as-is
				buffer.WriteString(string(BoLAsBytes))
				buffer.WriteString("}")
				bArrayMemberAlreadyWritten = true
			}
		}
	}
	return shim.Success(buffer.Bytes())
}

func (t *SimpleChaincode) queryVermas(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var A string // Entities
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}

	A = args[0]

	// Get the state from the ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	if Avalbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + A + "\",\"Amount\":\"" + string(Avalbytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return shim.Success(Avalbytes)
}

func (t *SimpleChaincode) queryBoL(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var A string // Entities
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}

	A = args[0]

	// Get the state from the ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	if Avalbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + A + "\",\"Amount\":\"" + string(Avalbytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return shim.Success(Avalbytes)
}

// Deletes an entity from state
func (t *SimpleChaincode) delete(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	A := args[0]

	// Delete the key from the state in ledger
	err := stub.DelState(A)
	if err != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}
